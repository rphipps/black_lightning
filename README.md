# Project Black Lightning

The aim of this project is to build a new website for Bedlam Theatre.
But not just that! It is to build a stable platform upon which new
heights can be reached.

There is (apparently) some more information in the `doc/` directory.
You can generate documentation about the code using `rake doc:app`.

You may also find some information on the [project wiki](https://bitbucket.org/bedlamtheatre/black_lightning/wiki/browse/) - especially if you're interested in hacking.

## The Fine Print

This project is the work of Team Adjective-Noun, comprising:

* Hayden Ball <hayden@haydenball.me.uk>
* Tom Turner <tom@tomturner.org.uk>
* Craig Snowden <craig@craigsnowden.com>
* Lewis Eason <me@lewiseason.co.uk>

with support of the EUTC and is licensed under The MIT License as follows:

> The MIT License (MIT)
>
> Copyright (c) 2015 Team Adjective-Noun
>
> Permission is hereby granted, free of charge, to any person obtaining a copy
> of this software and associated documentation files (the "Software"), to deal
> in the Software without restriction, including without limitation the rights
> to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
> copies of the Software, and to permit persons to whom the Software is
> furnished to do so, subject to the following conditions:
>
> The above copyright notice and this permission notice shall be included in all
> copies or substantial portions of the Software.
>
> THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
> IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
> FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
> AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
> LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
> OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
> SOFTWARE.

---